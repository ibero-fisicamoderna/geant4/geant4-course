\section{EM Fields}

\begin{frame}
    \frametitle{Tracking EM Fields}

    \begin{itemize}
    		\item Divide the trajectory of the particle in "steps"
		\begin{itemize}
			\item Straight free-flight tracks between consecutive physics interactions		
		\end{itemize}
		\item In presence of EM fields, the free-flight part between interactions is not straight
		\begin{itemize}
			\item Change of direction (B-field) or energy (E-field)
			\item Effect of fields must be incorporated into the tracking algorithm which is very CPU-demanding
		\end{itemize}
    		\item Notice: most codes handle only weak fields
    		\begin{itemize}
    			\item An e\textsuperscript{-} at rest will not accelerate, no syncrothron radiation, no avalanche
    		\end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Tracking in fields}
	
	\begin{itemize}
		\item In order to propagate a particle inside a field the equation of motion of the particle in the field is integrated numerically
		\item In general this is best done using a Runge-Kutta (RK) method for the integration of ordinary differential equations
		\item Once the curved path is calculated, Geant4 breaks it up into
linear chord segments
	
		\begin{center}
			\includegraphics[scale=0.25]{resources/linear_chord}	
		\end{center}

		\item The chord segments are determined to closely approximate the curved path
		\begin{itemize}
			\item In some cases, one step could be split in several helix-turns
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Example: how to create a magnetic field: uniform}
	
	\begin{itemize}
		\item Uniform field in the entire world volume: easy recipe
		
		\begin{center}
			\includegraphics[scale=0.20]{resources/ex_mag_field_p1}	
		\end{center}
		
		\item In general, one can customize the precision of the stepper and method used for the numerical integration of the equations
		
		\begin{center}
			\includegraphics[scale=0.20]{resources/ex_mag_field_p2}	
		\end{center}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Example: how to create a magnetic field: non-uniform}
	
	\begin{itemize}
		\item Non-uniform field in the world volume
		\begin{itemize}
			\item Create a class, derived from G4MagneticField implementing $f(\vec{x},t)$
			
			\includegraphics[scale=0.35]{resources/ex2_mag_field_p1}	
		\end{itemize}
	\end{itemize}
	
	\begin{center}		
		\includegraphics[scale=0.4]{resources/ex2_mag_field_p2}	
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Example: how to create a local magnetic field}
	
	\begin{itemize}
		\item It is possible to define a field inside a logical volume (and its daughters)
		\begin{itemize}
			\item This can be done creating a local G4FieldManager and attaching it to a logical volume
		\end{itemize}
	\end{itemize}
	
	\begin{center}		
		\includegraphics[scale=0.20]{resources/ex3_mag_field}	
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Customization}
	
	\begin{itemize}
		\item A few parameters to customize the precision of the tracking in EM fields. Most critical: "miss distance"
		
		\begin{itemize}
			\item Upper bound for the value of the sagitta (default: 3 mm)
			\item May be highly CPU consuming
			\begin{center}		
				\includegraphics[scale=0.20]{resources/linear_chord2}	
			\end{center}						
		\end{itemize}
		
		\item Integration calculated by 4th-order Runge-Kutta (G4ClassicalRK4), robust and general purpose
		\begin{itemize}
			\item If the field is not smooth (e.g. field map), lower-order (and
faster) integrators can be appropriate
			\item 3rd order G4SimpleHeum, 2nd order G4ImplicitEuler, 1st order G4ExplicitEuler
		\end{itemize}
	\end{itemize}
\end{frame}

\section{Replicas and parametrized volumes}

\begin{frame}
	\frametitle{Physical volumes}
	
	\begin{itemize}
		\item Placement volume (G4PVPlacement): one positioned volume
		\begin{itemize}
			\item One physical volume represents one "real" volume		
		\end{itemize}
		\item Repeated volume: a volume placed many times		
		\begin{itemize}
			\item One physical volume represents any number of "real" volumes
		\end{itemize}
		\item Parametrized (repetitions w.r.t. copy number)
		\item Replicas and Divisions
		\item Notice: a repeated volume is not equivalent to a loop of placements
	\end{itemize}	
\end{frame}

\begin{frame}
	\frametitle{Replicated volumes (G4PVReplica)}
	
	\begin{columns}
        \column{0.7\textwidth}
		\begin{itemize}
			\item The mother volume is completely filled with replicas, all having same size and shape
			\begin{itemize}
				\item If you need gaps, use G4PVDivision instead (less CPU-efficient)
			\end{itemize}
			\item Replication may occur along:
			\begin{itemize}
				\item Cartesian axes (kXAxis, kYAxis, kZAxis)
				\item Radial axis (cilindrical polar) (kRho) - onion rings
				\item Phi axis (cylindrical polar) (kPhi) – cheese wedges			
			\end{itemize}
		\end{itemize}
	    
        \column{0.3\textwidth}
        \includegraphics[scale=.30]{resources/replicated_volumes}       
    \end{columns}    	
\end{frame}

\begin{frame}
	\frametitle{G4PVReplica}
	
	\begin{columns}
        \column{0.8\textwidth}
        \includegraphics[scale=.25]{resources/replica_constructor}               
        
        \column{0.2\textwidth}
        \includegraphics[scale=.20]{resources/replica_phi}       
    \end{columns}    	
    
    Features and restrictions:
    \begin{itemize}
    		\item CSG solids only
    		\item G4PVReplica must be the only daughter
    		\item Replicas may be placed inside other replicas
    		\item Normal placement volumes may be placed inside replicas
    		\item No volume can be placed inside a radial replication
    		\item Parameterised volumes cannot be placed inside a replica
    	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Replica: axes, width and offset}	
	
	\begin{columns}
        \column{0.7\textwidth}
		Center of n th daughter is given as:
		
		\begin{itemize}
			\item Cartesian axes - kXaxis, kYaxis, kZaxis
	        \includegraphics[scale=.25]{resources/replicas_xyz}       
			\item Radial axis – kRho
	        \includegraphics[scale=.25]{resources/replicas_rho}       
			\item Phi axis – kPhi
	        \includegraphics[scale=.25]{resources/replicas_phi}       			
		\end{itemize}
        
        \column{0.3\textwidth}
        \includegraphics[scale=.20]{resources/replicas}       
    \end{columns}    	
\end{frame}

\begin{frame}
	\frametitle{G4PVDivision}
	
	\begin{columns}
        \column{0.7\textwidth}
        \begin{itemize}
			\item The G4PVDivision is similar to the G4PVReplica but
			\begin{itemize}
				\item Allows for gaps between mother and daughter volumes
				\item Less CPU-effective than replica
			\end{itemize}
			
			\item Shape of all daughter volumes must be the same as of the mother volume
			\item A number of shapes / axes patterns are
supported, e.g.
			\begin{itemize}
				\item G4Box : kXAxis, kYAxis, kZAxis
				\item G4Tubs : kRho, kPhi, kZAxis
				\item G4Cons : kRho, kPhi, kZAxis
				\item ...
			\end{itemize}
		\end{itemize}
	
        \column{0.3\textwidth}
        \includegraphics[scale=.20]{resources/divisions}       
    \end{columns}    			
\end{frame}

\begin{frame}
	\frametitle{Parametrized volumes (G4VPVParameterisation)}
	
	Repeated volumes can differ by size, shape, material and transformation matrix, that can all be parameterised by the user as a function of the copy number\linebreak
	
	User is asked to derive her/his own parameterisation class from the G4VPVParameterisation class implementing the methods:
	
	\includegraphics[scale=.25]{resources/parametrized}    
\end{frame}

\begin{frame}
	\frametitle{Parametrized volumes}
	
	\begin{itemize}
		\item All daughters must be fully contained in the mother
		\begin{itemize}
			\item Daughters should not overlap to each other
		\end{itemize}
		
		\item Limitations:
		\begin{itemize}
			\item Applies to simple CSG solids only
			\item Grand-daughter volumes allowed only for special cases			
		\end{itemize}
		
		\item Typical use-cases:
		\begin{itemize}
			\item Complex detectors with large repetition of volumes, regular or irregular
			\item Medical applications: the material in tissue is modeled as parametrixed voxels with variable density
			\item Limited memory footprint
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{G4PVParametrized}
	
	\includegraphics[scale=.25]{resources/parameterised_const}
	
	\begin{itemize}
		\item Replicates the volume nReplicas times using the parameterization pParam, within the mother volume pMother
		\item pAxis specifies the tracking optimisation algorithm to apply:
		\begin{itemize}
			\item kXAxis, kYAxis, kzAxis - 1D voxelisation algorithm
			\item kUndefined - 3-D voxelisation algorithm
		\end{itemize}
		\item Each replicated volume is a touchable detector element
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Assembly \& reflections}
	
	\begin{itemize}
		\item Possible to represent a regular pattern of positioned volumes, composing a more or less complex structure
		\begin{itemize}
			\item structures which are hard to describe with simple replicas or parameterised volumes
		\end{itemize}
		\item Assembly volume (G4AssemblyVolume)
		\begin{itemize}
			\item acts as an envelope for its daughter volumes
		\end{itemize}
		
		\includegraphics[scale=.25]{resources/assembly}
		
		\item G4ReflectedSolid (derived from G4VSolid)
		
		\begin{itemize}
			\item Utility class representing a solid shifted from its original reference frame to a new mirror symmetric one
		\end{itemize}
	\end{itemize}
\end{frame}