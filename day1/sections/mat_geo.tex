\section{Materials}

\begin{frame}
    \frametitle{User classes}

    \begin{center}
        \includegraphics[scale=0.22]{resources/user_classes_mat_geo}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Units}

    \begin{itemize}
        \item Always specify the units (mm, cm, m, etc..). These units can be found in the CLHEP library (included in Geant4).
        \item You can also define you own units, however this will not be shown here.
        \item In order to output data in terms of a specific unit. You must divide the value by the unit you're interested in using e.g: G4cout << dE / MeV << " (MeV)" << G4endl
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{System of Units}

	All units defined in Geant4 come from the basic ones.

    \begin{center}
        \includegraphics[scale=0.25]{resources/basic_units}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Materials}

	Different levels of material description:

    \begin{itemize}
    	\item Isotopes: G4Isotope
    	\item Elements: G4Element
    	\item Molecules, compounds and mixtures: G4Material    	
    \end{itemize}
    
    \vspace{0.5cm}
    
    Attributes associated: temperature, pressure, state, density.
    G4Isotope and G4Element: describes properties of atoms.
    G4Material: describes the macroscopic properties of matter.    	
\end{frame}

\begin{frame}
    \frametitle{Creating Elements}

	Isotopes can be assembled into elements.
	
	\begin{center}
        \includegraphics[scale=0.25]{resources/isotope_const}
    \end{center}
    
    Elements:
    
    \begin{center}
        \includegraphics[scale=0.25]{resources/element_const}
    \end{center}
    	
\end{frame}

\begin{frame}
    \frametitle{Creating Elements}
    
    Elements can also be specified with a natural isotopic abundance.
    
    \begin{center}
        \includegraphics[scale=0.25]{resources/element_natur_const}
    \end{center}
    	
\end{frame}

\begin{frame}
    \frametitle{Elements and compounds}
    
    Single-element material
	    
    \begin{center}
        \includegraphics[scale=0.25]{resources/mat_selem_const}
    \end{center}
    
    Molecule material (composition by number of atoms)
    \begin{center}
        \includegraphics[scale=0.25]{resources/molecule_material_const}
    \end{center}    
    
\end{frame}

\begin{frame}
    \frametitle{Mixtures}
    
    By fraction of mass
	    
    \begin{center}
        \includegraphics[scale=0.23]{resources/mixture_frac_const}
    \end{center}
    
    Mixing various elements and materials
    
    \begin{center}
        \includegraphics[scale=0.23]{resources/mixture_const}
    \end{center}    
    
\end{frame}

\begin{frame}
    \frametitle{Example: Gas}
    
    It's necessary to specify the temperature and pressure since this affects the energy variation along a distance this is known as thermal scattering.
    
    \begin{center}
        \includegraphics[scale=0.25]{resources/gas_const}
    \end{center}
    
    Absolute vacuum doesn't exist (gas at very low density). Geant4 disallows creating materials with density equal to 0
    
    \begin{center}
        \includegraphics[scale=0.25]{resources/vacuum_const}
    \end{center}
    
\end{frame}

\begin{frame}
    \frametitle{NIST material database}
    
    \begin{itemize}
    	\item It's easy to retrieve predefined elements or materials:
    	
    	\begin{center}
	        \includegraphics[scale=0.25]{resources/nist_example}
    	\end{center}
    	
    	\item UI commands:
    	
    	\begin{center}
	        \includegraphics[scale=0.25]{resources/nist_ui}
    	\end{center}
    \end{itemize}
    
\end{frame}

\begin{frame}
    \frametitle{NIST material database}
    
	\begin{columns}
        \column{0.5\textwidth}

        \begin{itemize}
	    	\item NIST database for materials is imported inside Geant4
    		\item UI commands specific for handling materials
    		\item Precise values for the most relevant parameters:
	    	\begin{itemize}
    			\item Density
    			\item Mean excitation potential
    			\item Chemical bonds
	    		\item Element composition
    			\item Isotope composition
    			\item Various corrections
    		\end{itemize}
	    \end{itemize}
	    
        \column{0.5\textwidth}

        \includegraphics[scale=.25]{resources/nist_example_si}
        
        \begin{itemize}
	    	\item Natural isotope composition
    		\item More than 3000 isotope masses
	    \end{itemize}
    \end{columns}        
\end{frame}

\begin{frame}
    \frametitle{NIST material database}
    
	\begin{columns}
        \column{0.5\textwidth}

		\includegraphics[scale=.35]{resources/elementary_materials_nist}       
	    
        \column{0.5\textwidth}

        \includegraphics[scale=.30]{resources/compounds_nist}       
    \end{columns}    
\end{frame}

\section{Geometry}

\begin{frame}
    \frametitle{Describing our detector}
    
	\begin{itemize}
		\item A detector geometry is made of many volumes.
		\item The largest volume is called World volume; this volume contains all other volumes.
		\item You must derive your detector construction from the G4VUserDetectorConstruction abstract class.
		\item Implementing the pure virtual method Construct()
		\begin{itemize}
			\item Define shapes/solids required to describe the geometry
			\item Construct all necessary materials
			\item Construct and place volumes of you detector geometry
			\item (Optional) Define "sensitivity" properties associated to volumes
			\item (Optional) Associate magnetic field to detector regions
			\item (Optional) Define visualization attributes for the detector elements
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Geometry basics}
    
	\begin{itemize}
		\item Implement a class inheriting from the abstract base class G4VUserDetectorConstruction.
		
		\includegraphics[scale=.20]{resources/detector_construction_class}
		
		\item Create an instance of your implementation in the main program and add it to the run manager.

		\includegraphics[scale=.20]{resources/dc_impl_register}		
		
		\item Don't try to build all your geometry in just one class. This is OK if your geometry is simple, but if it's complex consider defining it in many classes.
		\item Don't delete your detector instance because the run manager does this automatically.
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{G4VUserDetectorConstruction}

	Construct()
	\begin{itemize}
		\item Define materials
		\item Define solids and volumes of the geometry
		\item Build the tree hierarchy of volumes
		\item Define visualization attributes
		\item (The most important) MUST return the world physical volume
	\end{itemize}
	
	ConstructSDAndField()
	\begin{itemize}
		\item Assign magnetic field to volumes / regions
		\item Define sensitive detectors and assign them to volumes
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Three conceptual layers}
	
	\begin{itemize}
		\item G4VSolid: Shape and size
		\item G4LogicalVolume: Hierarchy of volumes, material, sensitivity and magnetic field
		\item G4VPhysicalVolume: Position, rotation. The same logical volume can be placed many times (Repeated modules)				
	\end{itemize}

	\includegraphics[scale=.25]{resources/dc_layers}	
	
\end{frame}

\begin{frame}
	\frametitle{Detector geometry}

	\begin{center}
			\includegraphics[scale=.2]{resources/dg_s1}	
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Detector geometry}

	\begin{center}
			\includegraphics[scale=.2]{resources/dg_s2}	
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Detector geometry}

	\begin{center}
			\includegraphics[scale=.2]{resources/dg_s3}	
	\end{center}
\end{frame}

\begin{frame}
    \frametitle{Solids}
    
	\begin{columns}
        \column{0.75\textwidth}

		\begin{itemize}
			\item CSG (Constructed Solid Geometry) solids: G4Box, G4Tubs, G4Cons, G4Trd, ...
			\item Specific solids (CSG like): G4Polycone, G4Polyhedra, G4Hype, ...
			\item BREP (Boundary REPresented) solids: G4BREPSolidPolycone, G4BSplineSurface, ...
			\item Boolean solids: G4UnionSolid, G4SubstractionSolid, ...
		\end{itemize}
	    
        \column{0.25\textwidth}

        \includegraphics[scale=.2]{resources/solids}       
    \end{columns}    
\end{frame}

\begin{frame}
    \frametitle{G4Box}
    
	\includegraphics[scale=.2]{resources/g4box}       
\end{frame}

\begin{frame}
    \frametitle{G4Tubs \& G4Cons}
    
	\includegraphics[scale=.2]{resources/g4tubs_cons}       
\end{frame}

\begin{frame}
    \frametitle{More CSG Solids}
    
	\includegraphics[scale=.2]{resources/other_csg_solids}       
\end{frame}

\begin{frame}
    \frametitle{Boolean Solids}
    
    \begin{columns}
        \column{0.75\textwidth}

		\begin{itemize}
			\item Solids can be combined using boolean operations.
			\begin{itemize}
				\item G4UnionSolid, G4SubtractionSolid, G4IntersectionSolid
				\item Requires: 2 solids, 1 boolean operation and an (optional) transformation for the 2nd solid.
				\item 2nd solid is positioned relative to the coordinate system of the 1st solid.
				\item Result of boolean operations becomes a solid, this solid is reusable with another boolean operation.
			\end{itemize}						
			\item Solids to be combined can be either CSG or other Boolean solids.
		\end{itemize}
	    
        \column{0.25\textwidth}

        \includegraphics[scale=.20]{resources/boolean_solids}
    \end{columns}          
\end{frame}

\begin{frame}
    \frametitle{Boolean Solids - Example}
    
    \begin{center}
    	\includegraphics[scale=.25]{resources/boolean_solids_ex}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Logical Volumes}
    
    \begin{itemize}
    	\item Contains all information of volume except position:
    	\begin{itemize}
    		\item Shape and dimension (G4VSolid)
    		\item Material, sensitivity, visualization attributes
    		\item Position of daughter volumes
    		\item Magnetic field, User limits
    	\end{itemize}
    	\item Physical volumes of same type can share a logical volume.
    \end{itemize}
    
    \begin{center}
    	\includegraphics[scale=.25]{resources/logical_volume_const}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Physical Volumes}
    
    \begin{columns}
        \column{0.75\textwidth}

		\begin{itemize}
			\item A physical volume is a positioned instance of a logical volume inside another logical volume (the containing logical volume is called the mother volume)
			\item Placement (G4PVPlacement)
			\item Repeated: a volume placed many times
			\begin{itemize}
				\item can represent many volumes
				\item reduces memory
				\item G4PVReplica (= simple repetition)
				\item G4PVParameterised (= more complex pattern)
				\item G4PVDivision
			\end{itemize}
		\end{itemize}
	    
        \column{0.25\textwidth}

        \includegraphics[scale=.22]{resources/physical_volumes}
    \end{columns}          
\end{frame}

\begin{frame}
    \frametitle{Geometry Hierarchy}
    
    \begin{itemize}
    	\item A volume is placed in its mother volume
    	\begin{itemize}
    		\item Position and rotation of the daughter volume is described with resprect to the local coordinate system of the mother volume
    		\item The origin of the mother's local coordinate system is at the center of the mother volume
    		\item Daughter volumes cannot protrude from the mother volume
    		\item Daughter volumes cannot overlap    		
    	\end{itemize}
    	\item The logical volume of mother knows the daughter volumes is contains
    	\begin{itemize}
    		\item It is uniquely defined to be their mother volume.
    	\end{itemize}
    \end{itemize}
    
    \begin{center}
    	    \includegraphics[scale=.30]{resources/geometry_hierarchy}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Geometry Hierarchy}
    
	\begin{columns}
        \column{0.70\textwidth}

		\begin{itemize}
	    	\item One logical volume can be placed more than once
    		\item The mother daughter relationship is contained in the G4LogicalVolume
    		\item The world volume must be a unique physical volume which fully contains all other volumes (root volume of the hierarchy).    	
	    	\begin{itemize}
    			\item The world volume defines the global coordinate system. The origin of the global coordinate system is at the center of the world volume.
    			\item Position of a track is given with respect to the global coordinate system
	    	\end{itemize}
    	\end{itemize}
	    
        \column{0.30\textwidth}

        \includegraphics[scale=.20]{resources/geometry_hierarchy_2}
    \end{columns}                 
\end{frame}

\begin{frame}
    \frametitle{G4PVPlacement}
    
	\begin{itemize}
		\item Single volume positioned relatively to the mother volume
		\begin{itemize}
			\item In a frame rotated and translated relative to the coordinate system of the mother volume						
		\end{itemize}
		\item A few variants
		\begin{itemize}
			\item Using G4Transform3D to represent the direct rotation and translation of the solid instead of the frame.
			\item Specifying the mother volume as a pointer to its physical volume instead of its logical volume.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{G4PVPlacement: Rotation of the mother frame}
    
    \begin{center}
		\includegraphics[scale=.225]{resources/g4pvplacement_const}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{G4PVPlacement: Rotation in the mother frame}
    
    \begin{center}
		\includegraphics[scale=.225]{resources/g4pvplacement_const_2}
    \end{center}
\end{frame}