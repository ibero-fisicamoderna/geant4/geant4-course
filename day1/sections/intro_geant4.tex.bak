\section{Introduction to Geant4}

\begin{frame}
    \frametitle{Toolkit and User Application}

    \begin{itemize}
        \item It's a toolkit!
            \begin{itemize}
                \item You can't run it as you would any other application.
                \item You have to make use of the tools in order for it to run.
            \end{itemize}
        \item Consequences:
            \begin{itemize}
                \item There is no such thing as "Geant4 defaults".
                \item You must provide the necessary information to configure you simulation.
                \item You must deliberately choose which Geant4 tools to use.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Basic Concepts}

    \begin{itemize}
        \item What you must do:
            \begin{itemize}
                \item Describe your experimental set-up.
                \item Provide the primary particles input to your simulation.
                \item Decide which particles and physics models you want to use out of those available in Geant4 and the precision of your simulation (cuts to produce and track secondary particles).
            \end{itemize}
        \item You may also want:
            \begin{itemize}
                \item To interact with Geant4 kernel to control your simulation.
                \item To visualize your simulation configuration or results.
                \item To produce histograms, tuples, etc.. to be further analyzed.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Main Geant4 Capabilities}

    \begin{itemize}
        \item Transportation of a particle `step-by-step` taking into account all possible interactions with materials and fields.
        \item The transport ends if the particle
            \begin{itemize}
                \item is slowed down to zero kinetic energy (and doesn't have any interaction at rest)
                \item disappears in some interaction (annihilated)
                \item reaches the end of the simulation volume.
            \end{itemize}
        \item Geant4 allows the user to access the transportation process and retrieve the results (User Actions):
            \begin{itemize}
                \item at the beginning and end of the transport (event).
                \item at the end of each step in transportation.
                \item if the particle reaches a sensitive detector.
                \item Others...
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Multi-thread mode}

    \begin{itemize}
        \item Geant4 10.0 (released Dec, 2013) supports multi-thread approach for multi-core machines.
            \begin{itemize}
                \item Simulation is automatically split on an event-by-event basis.
                    \begin{itemize}
                        \item different events are processed by different cores
                    \end{itemize}
                \item Can fully profit of all cores available on modern machines. This means we obtain a substantial boost in our simulations.
                \item Unique copy (master) of geometry and physics.
                    \begin{itemize}
                        \item All cores have them with read-only access.
                    \end{itemize}
            \end{itemize}
        \item Backwards compatible with the sequential mode.
            \begin{itemize}
                \item Multi-thread programming requires thread-safe operations.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Multi-thread}

    \includegraphics[scale=0.3]{resources/mt_geant4}
\end{frame}

\begin{frame}
    \frametitle{Parallelism}

    \begin{center}
        \includegraphics[scale=0.3]{resources/p_geant4}
    \end{center}
\end{frame}
